package com.example.delivery.core.domain.shared.kernel;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class LocationTest {

    @Nested
    class CreateLocationTest {

        @Test
        void create_whenCoordinateIsCorrect_thenCreate() {

            var actual = Location.create(1, 2);

            assertAll(
                () -> assertEquals(1, actual.abscissaValue()),
                () -> assertEquals(2, actual.ordinateValue())
            );
        }


        @Test
        void create_whenCoordinateIsNotCorrect_thenThrow() {
            var actual = assertThrows(IllegalArgumentException.class, () -> Location.create(0, 2));


            assertEquals("Expected coordinate should be between '1' and '10'. Actual coordinate is '0'.", actual.getMessage());
        }

    }


    @Test
    void distanceTo() {
        var currentLocation = Location.create(2, 3);
        var targetLocation = Location.create(8, 9);


        var actual = currentLocation.distanceTo(targetLocation);


        assertEquals(12, actual);
    }

}
