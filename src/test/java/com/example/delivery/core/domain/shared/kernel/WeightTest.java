package com.example.delivery.core.domain.shared.kernel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class WeightTest {

    @Nested
    class CreateWeightTest {


        @Test
        void create_whenWightIsCorrect_thenCreate() {
            var actual = Weight.create(10);

            assertEquals(10, actual.value());
        }


        @Test
        void create_whenWightIsNotCorrect_thenThrow() {
            var actual = assertThrows(IllegalArgumentException.class, () -> Weight.create(-10));


            assertEquals("Expected weight should be positive. Actual weight is '-10'.", actual.getMessage());
        }

    }

    @Nested
    class CompareToWeightTest {

        @Test
        void compareTo_whenFirstIsHeavier_thenReturnPositive() {
            var first = Weight.create(5);
            var second = Weight.create(1);


            var actual = first.compareTo(second);


            assertEquals(1, actual);
        }


        @Test
        void compareTo() {
            var first = Weight.create(1);
            var second = Weight.create(1);


            var actual = first.compareTo(second);


            assertEquals(0, actual);
        }


        @Test
        void compareTo_whenFirstIsLighter_thenReturnNegative() {
            var first = Weight.create(1);
            var second = Weight.create(5);


            var actual = first.compareTo(second);


            assertEquals(-1, actual);
        }

    }
}
