/**
 * Contains the Kafka listeners and message processors that handle Kafka messages,
 * converting them into input suitable for the core business logic.
 */

package com.example.delivery.api.adapters.kafka;