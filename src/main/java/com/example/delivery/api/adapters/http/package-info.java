/**
 * Contains the HTTP controllers that handle HTTP requests,
 * converting them into input suitable for the core business logic.
 */

package com.example.delivery.api.adapters.http;