/**
 * Contains the inbound adapters that handle incoming data from external systems
 * and transform it into a format understood by the core application.
 */

package com.example.delivery.api.adapters;