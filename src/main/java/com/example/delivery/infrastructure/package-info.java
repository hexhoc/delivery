/**
 * Manages outgoing interactions with databases, messaging systems, file systems,
 * third-party services, and other low-level operations that support the application's
 * primary business features.
 */
package com.example.delivery.infrastructure;