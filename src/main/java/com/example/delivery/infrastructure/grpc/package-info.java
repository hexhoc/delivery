/**
 * The grpc package contains classes and components responsible for managing
 * gRPC (Google Remote Procedure Call) services.
 */

package com.example.delivery.infrastructure.grpc;