/**
 * Contains classes related to sending message to Kafka
 * ensuring proper setup of topics, producers, etc.
 */

package com.example.delivery.infrastructure.kafka;