/**
 * package is responsible for managing postgresql interactions.
 * It includes repositories, entities
 */

package com.example.delivery.infrastructure.postgres;