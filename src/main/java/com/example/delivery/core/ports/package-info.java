/**
 * Contains the ports (interfaces) that define the boundaries between the core
 * business logic (use cases) and external systems (infrastructure).
 */
package com.example.delivery.core.ports;