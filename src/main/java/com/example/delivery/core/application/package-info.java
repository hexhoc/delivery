/**
 * The application package encapsulates the application's use cases and
 * orchestration logic. It serves as a mediator between the presentation layer
 * and the core business logic, handling commands, queries, and their associated
 * handlers.
 */

package com.example.delivery.core.application;