/**
 * The core package contains all the essential business logic and domain entities
 * of the application. This package is responsible for encapsulating the core
 * business rules and logic without being concerned with the
 * implementation details of how external systems interact with it.
 */

package com.example.delivery.core;