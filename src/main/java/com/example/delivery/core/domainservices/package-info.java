/**
 * Contains the implementations of the services
 * to perform the application's business logic.
 */

package com.example.delivery.core.domainservices;