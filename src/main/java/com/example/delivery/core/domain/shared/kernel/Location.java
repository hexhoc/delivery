package com.example.delivery.core.domain.shared.kernel;

import static java.lang.String.format;

public record Location(int abscissaValue, int ordinateValue) {

    private static final int MAX_COORDINATE_VALUE = 10;
    private static final int MIN_COORDINATE_VALUE = 1;

    public static final Location MIN_LOCATION = create(MIN_COORDINATE_VALUE, MIN_COORDINATE_VALUE);
    public static final Location MAX_LOCATION = create(MAX_COORDINATE_VALUE, MAX_COORDINATE_VALUE);


    public Location {
        validateCoordinate(abscissaValue);
        validateCoordinate(ordinateValue);
    }


    public static Location create(int abscissaValue, int ordinateValue) {
        return new Location(abscissaValue, ordinateValue);
    }


    private static void validateCoordinate(int coordinate) {
        if (coordinate < MIN_COORDINATE_VALUE || coordinate > MAX_COORDINATE_VALUE) {
            throw new IllegalArgumentException(
                format("Expected coordinate should be between '%d' and '%d'. Actual coordinate is '%d'.",
                    MIN_COORDINATE_VALUE, MAX_COORDINATE_VALUE, coordinate)
            );
        }
    }


    public int distanceTo(Location targetlLocation) {
        int absAbscissaDelta = Math.abs(this.abscissaValue - targetlLocation.abscissaValue);
        int absOrdinateDelta = Math.abs(this.ordinateValue - targetlLocation.ordinateValue);

        return absAbscissaDelta + absOrdinateDelta;
    }

}