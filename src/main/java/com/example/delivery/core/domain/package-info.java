/**
 * Contains the core business entities used throughout the application (Aggregate,
 * Entity, ValueObject). These domain objects encapsulate the core business logic and rules.
 */
package com.example.delivery.core.domain;