package com.example.delivery.core.domain.courier.aggregate;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CourierStatus {

    NOT_AVAILABLE(1, "NotAvailable"),
    READY(2, "Ready"),
    BUSY(3, "Busy"),
    ;

    private final int id;
    private final String name;

}
