package com.example.delivery.core.domain.order.aggregate;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OrderStatus {

    CREATED(1, "Created"),
    ASSIGNED(2, "Assigned"),
    COMPLETED(3, "Completed");

    private final int id;
    private final String name;

}
